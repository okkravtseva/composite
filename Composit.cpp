#include <iostream>
#include <vector>
#include <string>
#include <memory>
#include <cassert>
#include <algorithm>
#include <random>
#include <numeric>

using namespace std;

class password_generator
{
public:
//	password_generator() = default;
//	password_generator(const password_generator&) = default;
//	password_generator& operator=(const password_generator&) = default;
	virtual ~password_generator() {}

	virtual string generate() = 0; // generation of new random string
	virtual size_t length() = 0; // minimum length of generated string
	virtual string allowed_chars() = 0; // returns the string with all allowed chars for concrete generator
	virtual void add(unique_ptr<password_generator> &pg) // adds unit of password part's generator
	{
		assert(false);
	}
};

class basic_password_generator : public password_generator
{
public:
	basic_password_generator() { min_length = 0; }
	basic_password_generator(size_t length) { min_length = length; }

	inline void set_length(size_t length) { min_length = length; }
	size_t length() { return min_length; }

	size_t min_length;
};

// generation of new random string:
string generate(const char* character_set, size_t set_length, size_t min_len, size_t max_len)
{
	static random_device len_rnd;
	static mt19937 len_rd(len_rnd());

	uniform_int_distribution<size_t> len_dist(min_len, max_len);
	size_t len = len_dist(len_rd); // random length of new string

	random_device sym_rnd;
	mt19937 sym_rd(sym_rnd());
	uniform_int_distribution<size_t> sym_dist(0, set_length - 1);

	string res;
	for (size_t i = 0; i < len; ++i)
	{
		res += character_set[sym_dist(sym_rd)]; // random symbol from character_set string
	}
	cout << "Simple generator: " << res << endl;
	return res;
}

class digit_generator : public basic_password_generator
{
public:
	digit_generator(size_t length = 2) { min_length = length; }

	string generate()
	{
		return ::generate(chars, chars_len, min_length, min_length + min_length);
	}

	string allowed_chars()
	{
		return chars;
	}
protected:
	static constexpr char chars[] = "0123456789";
	static constexpr size_t chars_len = (sizeof(chars))/sizeof(chars[0]) - 1;
};

class symbol_generator : public basic_password_generator
{
public:
	symbol_generator(size_t length = 1) { min_length = length; }

	string generate()
	{
		return ::generate(chars, chars_len, min_length, min_length + min_length);
	}
	string allowed_chars()
	{
		return chars;
	}
protected:
	static constexpr char chars[] = "!\"#$�&\'()*+,-./:;<=>?@[\\]^_`{|}~";
	static constexpr size_t chars_len = (sizeof(chars)) / sizeof(chars[0]) - 1;
};

class upper_letter_generator : public basic_password_generator
{
public:
	upper_letter_generator(size_t length = 3) { min_length = length; }

	string generate()
	{
		return ::generate(chars, chars_len, min_length, min_length + min_length);
	}
	string allowed_chars()
	{
		return chars;
	}
protected:
	static constexpr char chars[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	static constexpr size_t chars_len = (sizeof(chars)) / sizeof(chars[0]) - 1;
};

class lower_letter_generator : public basic_password_generator
{
public:
	lower_letter_generator(size_t length = 4) { min_length = length; }

	string generate()
	{
		return ::generate(chars, chars_len, min_length, min_length + min_length);
	}
	string allowed_chars()
	{
		return chars;
	}
protected:
	static constexpr char chars[] = "abcdefghijklmnopqrstuvwxyz";
	static constexpr size_t chars_len = (sizeof(chars)) / sizeof(chars[0]) - 1;
};

class composite_password_generator : public password_generator
{
public:
//	composite_password_generator() = default;
//	composite_password_generator(const composite_password_generator&) = default;
//	composite_password_generator& operator=(const composite_password_generator&) = default;
	~composite_password_generator() = default;

	string generate()
	{
		string res;

		vector<int> indexes(generators.size());
		iota(indexes.begin(), indexes.end(), 0);
		static random_device rd;
		static mt19937 g(rd());

		shuffle(indexes.begin(), indexes.end(), g);

		for (auto ind : indexes)
		{
			res += generators[ind]->generate();
		}
		cout << "Complex generator: " << res << endl;
		return res;
	}
	string allowed_chars()
	{
		string res;
		for (auto &basic_gen : generators)
		{
			res += basic_gen->allowed_chars();
		}
		return res;
	}

	size_t length()
	{
		size_t len = 0;
		for (auto& basic_gen : generators)
		{
			len += basic_gen->length();
		}
		return len;
	}
	virtual void add(unique_ptr<password_generator> &pg) // adds unit of password part's generator
	{
		generators.push_back(move(pg));
	}

	vector<unique_ptr<password_generator>> generators;
};

int main()
{
// Composite password generator digit_symb_gen. It uses special symbols and digits (see below):
	unique_ptr<password_generator> digit_symb_gen = make_unique<composite_password_generator>();
// Simple password generator (digits only). It can be created, for example, in this way:
	auto dg = unique_ptr<password_generator>(new digit_generator(3));
	digit_symb_gen->add(dg);
// Simple password generator (special characters). It can be created, for example, in this way:
	unique_ptr<password_generator> sg(new symbol_generator(2));
	digit_symb_gen->add(sg);
// Password generation using only special characters and numbers:
	digit_symb_gen->generate();

	cout << endl;

// Composite password generator upper_lower_gen. It uses upper and lower letters (see below)
	unique_ptr<password_generator> upper_lower_gen(new composite_password_generator());
// Simple password generator (upper letters only):
	unique_ptr<password_generator> ug(new upper_letter_generator(4));
	upper_lower_gen->add(ug);
// Simple password generator (lower letters only):
	unique_ptr<password_generator> lg(new lower_letter_generator(5));
	upper_lower_gen->add(lg);
// Password generation using only upper and lower letters:
	upper_lower_gen->generate();

	cout << endl;

// Composite password generator universal_gen. It uses all possible symbols:
	unique_ptr<password_generator> universal_gen(new composite_password_generator);
	universal_gen->add(digit_symb_gen);
	universal_gen->add(upper_lower_gen);
	universal_gen->generate();
}